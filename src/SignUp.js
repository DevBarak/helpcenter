import styled from "styled-components";
import React, { useState } from 'react';
import signupImg from './assets/signup.jpeg'
import arrow from './assets/arrowLeftBlack.svg'
import { size } from "./breakpoints";
import axios from "axios";

const SignUp = ({page, setPage}) => {

    const [fullname, setFullName] = useState('');
    const [address, setAddress] = useState('');
    const [phone, setPhone] = useState('');
    const [gender, setGender] = useState('');
    const [state, setState] = useState('');
    const [lg, setLg] = useState('');
    const [ward, setWard] = useState('');
    const [pollingUnit, setPollingUnit] = useState('');
    const [validid, setValidid] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    


    const handleSubmit = async (e) => {
        e.preventDefault();
    
        try {
          const response = await axios.post('http://actmovement.org/api/auth/register', {
            fullname,
            address,
            phone,
            gender,
            state,
            lg,
            ward,
            pollingUnit,
            validid,
            email,
            password,
            confirmPassword
          });
    
          console.log(response.data);
        } catch (error) {
          console.error("error");
        }
      };


    return ( 
        <Container>
           <header>
                <img src={arrow} alt="img" onClick={() => setPage(1)} />
                <h2>Personal Details</h2>
           </header>

           <Show>
                <div className="left">
                    <img src={signupImg} alt="img" />
                </div>

                <form onSubmit={handleSubmit}>
                    <div>
                        <label>Full Name</label>
                        <input type="text" placeholder="Enter full name here..."
                        value={fullname}
                        onChange={(e) => setFullName(e.target.value)} />
                    </div>
                    <div>
                        <label>Address</label>
                        <input type="text" placeholder="Enter address here..."
                        value={address}
                        onChange={(e) => setAddress(e.target.value)} />
                    </div>
                    <div>
                        <label>Phone</label>
                        <input type="text" placeholder="Enter mobile number here..."
                        value={phone}
                        onChange={(e) => setPhone(e.target.value)} />
                    </div>
                    <div>
                        <label>Gender</label>
                        <select value={gender}
                        onChange={(e) => setGender(e.target.value)}>
                            <option value="1">Select your gender</option>
                            <option value="2">Male</option>
                            <option value="3">Female</option>
                        </select>
                    </div>
                    <div>
                        <label>State</label>
                        <select value={state}
                        onChange={(e) => setState(e.target.value)}>
                            <option value="1">Select your state</option>
                            <option value="2">Lagos</option>
                            <option value="3">Ogun</option>
                            <option value="4">Ekiti</option>
                        </select>
                    </div>
                    <div>
                        <label>Local Government</label>
                        <select value={lg}
                        onChange={(e) => setLg(e.target.value)}>
                            <option value="1">Select your lg</option>
                            <option value="2">ijebu</option>
                            <option value="3">shagamu</option>
                            <option value="4">abeokuta</option>
                        </select>
                    </div>
                    <div>
                        <label>Ward</label>
                        <select value={ward}
                        onChange={(e) => setWard(e.target.value)}>
                            <option value="1">Select your ward</option>
                            <option value="2">aguda</option>
                            <option value="3">shagamu</option>
                            <option value="4">abeokuta</option>
                        </select>
                    </div>
                    <div>
                        <label>Polling Unit</label>
                        <select value={pollingUnit}
                        onChange={(e) => setPollingUnit(e.target.value)}>
                            <option value="1">Select your polling unit</option>
                            <option value="2">okeira</option>
                            <option value="3">agege</option>
                            <option value="4">ifako</option>
                        </select>
                    </div>
                    <div>
                        <label>Valid Id</label>
                        <select value={validid}
                        onChange={(e) => setValidid(e.target.value)}>
                            <option value="1">Select a valid ID Card</option>
                            <option value="2">idcard</option>
                            <option value="3">nincard</option>
                            <option value="4">voterscard</option>
                        </select>
                    </div>
                    <div>
                        <label>Email</label>
                        <input type="email" placeholder="Enter email here..."
                        value={email}
                        onChange={(e) => setEmail(e.target.value)} />
                    </div>
                    <div>
                        <label>Password</label>
                        <input type="password" placeholder="Enter password here..."
                        value={password}
                        onChange={(e) => setPassword(e.target.value)} />
                    </div>
                    <div>
                        <label>Confirm Password</label>
                        <input type="password" placeholder="Confirm Password..."
                        value={confirmPassword}
                        onChange={(e) => setConfirmPassword(e.target.value)} />
                    </div>
    

                    <button type="submit" onClick={() => setPage(3)}>Next</button>
                </form>
           </Show>
        </Container>
     );
}
 
const Container = styled.div`
    width: 100%;

    header {
        height: 100%;
        padding: 18px 24px;
        background: linear-gradient(to top, #432685, #582cc1, #792fcb);
        background-repeat: no-repeat;
        color: #fff;
        display: flex;
        align-items: center;
        gap: 10px;

        h2 {
            font-weight: 500;
            font-size: 25px;
        }
    }
`

const Show = styled.div`
    margin-top: 160px;
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 15%;
    padding-bottom: 150px;

    @media screen and (max-width: ${size.mobileL}) {
        flex-direction: column;
        margin-top: 60px;
    }

    .left {
        flex: .3;

        @media screen and (max-width: ${size.mobileL}) {
            display: none;
        }
        
       img {
            height: 300px;
            width: 80%;
       }
    }

    form {
        display: flex;
        flex-direction: column;
        flex: .4;

        @media screen and (max-width: ${size.mobileL}) {
            width: 80%;
        }

        div {
            margin-bottom: 20px;
            display: flex;
            flex-direction: column;
            gap: 10px;

            label {
                font-size: 18px;
                color: #432685;
                font-weight: 600;
            }

            input {
                width: 100%;
                border: none;
                background: #f4f4f4;
                border-radius: 10px;
                outline: none;
                padding: 18px 20px;

                @media screen and (max-width: ${size.mobileL}) {
                    width: 90%;
                }
            }

            select {
                width: 109%;
                border: none;
                background: #f4f4f4;
                border-radius: 10px;
                outline: none;
                padding: 18px 20px;

                @media screen and (max-width: ${size.mobileL}) {
                    width: 100%;
                }
            }

        }

        button {
            display: flex;
            align-items: center;
            justify-content: center;
            width: 109%;
            border: none;
            background: #432685;
            color: #fff;
            border-radius: 10px;
            outline: none;
            padding: 10px;
            font-size: 27px;

            
            @media screen and (max-width: ${size.mobileL}) {
                width: 100%;
            }
        }
   }
`


export default SignUp;