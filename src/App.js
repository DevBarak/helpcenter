import React, { useState } from 'react';
import Landing from './Landing';
import SignUp from './SignUp';
import Dashboard from './Dashboard';


function App() {

  const [page, setPage] = useState(1);

  return (
    <div className="App">
      {page==1 && <Landing page={page} setPage={setPage} />}
      {page==2 && <SignUp page={page} setPage={setPage} />}
      {page==3 && <Dashboard page={page} setPage={setPage} />}
    </div>
  );
}

export default App;
