import styled from "styled-components";
import React from "react";
import circles from '../assets/circles.png'
import { size } from '../breakpoints';

const Terms = () => {
    return ( 
        <Con>
            <div className="circle">
                <header>
                    <p>did not find what</p>
                    <h3>you were looking for?</h3>
                    <p id="small"> aliquet cursus. Suspendisse quis orci at leo tempor dapibus et</p>
                </header>

                <div className="btn">
                    <button>Yes, I did</button>
                    <button>No, I didn't</button>
                    <button>I prefer not to say</button>
                </div>
            </div>

            <svg id="line" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#582cc1" fill-opacity="1" d="M0,192L40,202.7C80,213,160,235,240,240C320,245,400,235,480,224C560,213,640,203,720,176C800,149,880,107,960,85.3C1040,64,1120,64,1200,80C1280,96,1360,128,1400,144L1440,160L1440,320L1400,320C1360,320,1280,320,1200,320C1120,320,1040,320,960,320C880,320,800,320,720,320C640,320,560,320,480,320C400,320,320,320,240,320C160,320,80,320,40,320L0,320Z"></path></svg>
        </Con>
     );
}

const Con = styled.div`
    width: 100%;
    padding-top: 400px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    text-align: center; 

    @media screen and (max-width: ${size.mobileL}) {
        padding-top: 100px;
    }

    .circle {
        height: 650px;
        width: 700px;
        background-image: url(${circles});
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        display: flex;
        flex-direction: column;
        justify-content: center;
        text-align: center;

        @media screen and (max-width: ${size.mobileL}) {
            height: 500px;
            width:500px;
        }

    }

    header {
        text-align: center;
        width: 100%;

        p {
            color: #fb840d;
            font-size: 30px;
            text-transform: uppercase;

            @media screen and (max-width: ${size.mobileL})  {
                font-size: 20px;
                padding-bottom: 10px;
            }
        }
        h3 {
            font-weight: 400;
            font-size: 40px;
            margin-top: -34px;
            text-transform: uppercase;
            color: #000;

            @media screen and (max-width: ${size.mobileL}) {
                font-size: 30px;
            }
        }
        #small {
            font-weight: 400;
            font-size: 15px;
            text-transform: uppercase;
            color: #000;
            margin-top: -40px;

            @media screen and (max-width: ${size.mobileL})  {
                font-size: 12px;
            }
        }
   }

   .btn {
    display: flex;
    align-items: center;
    gap: 30px;
    justify-content: center;


        button {
            padding: 15px 30px;
            border: none;
            color: #fff;
            outline: none;
            background: linear-gradient(to top, #432685, #582cc1, #792fcb);
            cursor: pointer;
            font-size: 20px;

            @media screen and (max-width: ${size.mobileL}) {
                padding: 7px 15px;
                font-size: 15px;
            }
        }
        button:hover {
            background: #fb840d;
            cursor: pointer;
        }
   }

   #line {
    @media screen and (max-width: ${size.mobileL}) {
        width: 100%;
    }
   }
`
 
export default Terms;