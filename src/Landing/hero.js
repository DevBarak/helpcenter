import styled from "styled-components";
import React from "react";
import support from '../assets/support.png'
import email from '../assets/email.png'
import hero from '../assets/hero.png'
import { size } from '../breakpoints';

const Hero = ({page, setPage}) => {
    return ( 
        <Con>
            <nav className="navbar">
                <div className="contact">
                    <div className="support">
                        <img src={support} alt="img" />
                        <p>Support</p>
                    </div>

                    <div className="email">
                        <img src={email} alt="img" />
                        <p>support@gmail.com</p>
                    </div>
                </div>
                <div className="right">
                    
                    <ul className="nav-list">
                        <li><a href="#">home</a></li>
                        <li><a href="#">about</a></li>
                        <li><a href="#">invest</a></li>
                        <li><a href="#">pac11</a></li>
                        <li><a href="#">blog</a></li>
                        <li><a href="#">contact</a></li>
                    </ul>

                    <button className="join" onClick={() => setPage(2)}>JOIN US</button>
                </div>
            </nav>

            <hr style={{borderColor: "#fb840d"}}/>

            <Showcase>
                <div className="left">
                    <h1>HELP CENTER</h1>
                    <p>Home&gt;Page&gt;Help Center</p>
                </div>
                <div className="right">
                    <img src={hero} alt="img" />
                </div>
            </Showcase>
        </Con>
     );
}

const Con = styled.div`
    width: 100%;
    height: 100%;
    background: linear-gradient(to top, #432685, #582cc1, #792fcb);
    background-repeat: no-repeat;

    // @media screen and (max-width: ${size.mobileL})

    .navbar {
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 24px;

        .contact {
            display: flex;
            align-items: center;

            .support {
                display: flex;
                align-items: center;
                gap: 7px;
                cursor: pointer;

                p {
                    color: #fff;
                    font-size: 14px;
                    font-weight: 700px;
                }
            }

            .email {
                display: flex;
                align-items: center;
                gap: 7px;
                margin-left: 10px;
                cursor: pointer;

                img {
                    height: 20px;
                }

                p {
                    color: #fff;
                    font-size: 14px;
                    font-weight: 700px;
                }
            }
        }

       .right {
        display: flex;
        align-items: center;
        gap: 200px;

        .nav-list {
            display: flex;
            align-items: center;
            gap: 40px;
            list-style: none;

            li {
                a {
                    text-decoration: none;
                    color: #fff;
                    text-transform: uppercase;
                    font-size: 15px;
                    font-weight: 400;
                }

                a:hover {
                    color: #fb840d;
                }
            }
        
            @media screen and (max-width: ${size.mobileL}) {
                display: none;
            }
        }

        .join {
            border: none;
            background: #fb840d;
            color: #fff;
            outline: none;
            padding: 20px 40px;
            border-radius: 10px;
            cursor: pointer;

            @media screen and (max-width: ${size.mobileL}) {
                padding: 10px 20px;
            }
        }

        .join:hover {
            border: 1px solid #fff;
            background: transparent;
        }
       }
    }
`

const Showcase = styled.div`
    padding: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 20%;

    @media screen and (max-width: ${size.mobileL}) {
        flex-direction: column;
        gap: 2%;
    }

    .left {
        color: #fff;

        h1 {
            font-size: 70px;
            font-weight: 400;

            @media screen and (max-width: ${size.mobileL}) {
               font-size: 50px;
            }
        }
        p {
            font-size: 20px;
            @media screen and (max-width: ${size.mobileL}) {
                font-size: 17px;
             }
        }
    }
`
 
export default Hero;