import styled from "styled-components";
import React from "react";
import email from '../assets/email.png'
import { size } from '../breakpoints';

const Footer = () => {
    return ( 
        <Con>
             <header>
                <p>subscribe to</p>
                <h3>our newsletter</h3>
                <p id="small"> aliquet cursus. Suspendisse quis orci at leo tempor dapibus et</p>
            </header>

            <Sub>
                <div>
                    <img src={email} alt="" />
                    <input type="email" placeholder="Enter Your Email Address"  />
                </div>

                <div className="btn">
                    <button>subscribe</button>
                </div>
            </Sub>

            <hr style={{borderColor: "#fb840d", marginTop: "200px", marginBottom: "30px", width: "90%"}}/>

            <ul className="nav-list">
                <li><a href="#">about us</a></li>
                <li><a href="#">contact</a></li>
                <li><a href="#">career</a></li>
                <li><a href="#">support</a></li>
                <li><a href="#">privacy policy</a></li>
            </ul>
        </Con>
     );
}

const Con = styled.div`
    background: linear-gradient(to top, #792fcb, #582cc1);
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    header {
        text-align: center;
        width: 100%;

        p {
            color: #fb840d;
            font-size: 30px;
            text-transform: uppercase; 

            @media screen and (max-width: ${size.mobileL}) {
                font-size: 20px;
                padding-bottom: 5px;
            }
        }
        h3 {
            font-weight: 400;
            font-size: 40px;
            margin-top: -34px;
            text-transform: uppercase;
            color: #fff;

            @media screen and (max-width: ${size.mobileL}) {
                font-size: 25px;
            }
        }
        #small {
            font-weight: 400;
            font-size: 15px;
            text-transform: lowercase;
            color: #fff;
            margin-top: -40px;

            @media screen and (max-width: ${size.mobileL}) {
                font-size: 12px;
                margin-top: 10px;
            }
        }
   }


//    footer nav

ul {
    display: flex;
    gap: 40px;
    list-style: none;

    @media screen and (max-width: ${size.mobileL}) {
        gap: 20px;
        align-items: center;
        justify-content: center;
    }

    li {
        a {
            color: #fff;
            text-transform: capitalize;
            font-size: 18px;
            text-decoration: none;

            @media screen and (max-width: ${size.mobileL}) {
                font-size: 12px;
            }
        }
    }
}
`

const Sub = styled.div`
   border: 1px solid #f5f5f5;
   width: 50vw;
   margin-top: 50px;
   margin-bottom: 50px;
   border-radius: 10px;
   display: flex;
   justify-content: space-between;
   gap: 2%;

   @media screen and (max-width: ${size.mobileL}) {
    width: 90vw;
}

   >div  {
    display: flex;
    width: 80%;
    padding: 10px;

    img {
        height: 50px;

        @media screen and (max-width: ${size.mobileL}) {
            height: 35px;
        }
    }
    input {
        width: 150%;
        border: none;
        outline: none;
        background: transparent;
        color: #fff;
        font-size: 17px;

        @media screen and (max-width: ${size.mobileL}) {
            font-size: 14px;
        }
    }

    input::placeholder {
        color: #fff;
        font-size: 17px;

        @media screen and (max-width: ${size.mobileL}) {
            font-size: 14px;
        }
    }
   }

   .btn {
    width: 20%;
    background: #fb840d;
    display: flex;
    justify-content: center;
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
    cursor: pointer;

    button {
        background: transparent;
        border: none;
        outline: none;
        color: #fff;
        font-size: 20px;
        text-transform: uppercase;
        cursor: pointer;

        @media screen and (max-width: ${size.mobileL}) {
            font-size: 14px;
        }
    }
   }
`
 
export default Footer;