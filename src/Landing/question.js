import styled from "styled-components";
import React from "react";
import search from '../assets/search.png'
import { size } from '../breakpoints';


const Question = () => {

    const quearray = [
        {
            head: "Lorem Ipsum Dolor Amet",
            content: "Praesent commodo laoreet lorem, vel congue ex auctor nec. Aenean lobortis nunc lobortis turpis viverra, ut commodo ligula congue. Proin faucibus nisi nibh, eget vulputate dui sagittis at. "
        },
        {
            head: "Lorem Ipsum Dolor Amet",
            content: "Praesent commodo laoreet lorem, vel congue ex auctor nec. Aenean lobortis nunc lobortis turpis viverra, ut commodo ligula congue. Proin faucibus nisi nibh, eget vulputate dui sagittis at. "
        },
        {
            head: "Lorem Ipsum Dolor Amet",
            content: "Praesent commodo laoreet lorem, vel congue ex auctor nec. Aenean lobortis nunc lobortis turpis viverra, ut commodo ligula congue. Proin faucibus nisi nibh, eget vulputate dui sagittis at. "
        },
        {
            head: "Lorem Ipsum Dolor Amet",
            content: "Praesent commodo laoreet lorem, vel congue ex auctor nec. Aenean lobortis nunc lobortis turpis viverra, ut commodo ligula congue. Proin faucibus nisi nibh, eget vulputate dui sagittis at. "
        },
        {
            head: "Lorem Ipsum Dolor Amet",
            content: "Praesent commodo laoreet lorem, vel congue ex auctor nec. Aenean lobortis nunc lobortis turpis viverra, ut commodo ligula congue. Proin faucibus nisi nibh, eget vulputate dui sagittis at. "
        },
        {
            head: "Lorem Ipsum Dolor Amet",
            content: "Praesent commodo laoreet lorem, vel congue ex auctor nec. Aenean lobortis nunc lobortis turpis viverra, ut commodo ligula congue. Proin faucibus nisi nibh, eget vulputate dui sagittis at. "
        },
        
    ]
    return ( 
        <Con>
            <Search>
               <div>
                <input type="text" placeholder="Search Our Help Center" />
               </div>
                <div className="btn">
                 <button><img src={search} alt="img" />SEARCH</button>
                </div>
            </Search>
            <Box>
                <header>
                    <p>Got any questions</p>
                    <h3>We've got answers</h3>
                </header>

               <div className="que">
                    {quearray.map(({content, head}) => 
                        <div className="que-box">
                            <h4>{head}</h4>
                            <p>{content}</p>
                        </div>
                    )}
               </div>
            </Box>
        </Con>
     );
}

const Con = styled.div`
    width: 100%;
    height: 100%;
    background: #fff;
    display: flex;
    align-items: center;
    flex-direction: column;
    justify-content: center;

    // @media screen and (max-width: ${size.mobileL})
`

const Search = styled.div`
    display: flex;
    gap: 20px;
    align-items: center;
    border: 1px solid none;
    margin-top: -30px;
    background: #fff;
    border-radius: 10px;
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);

    input {
        width: 500px;
        outline: none;
        border: none;
        padding: 20px 30px;

        @media screen and (max-width: ${size.mobileL}) {
            width: 150px;
            padding: 10px 20px;
        }
    }

   .btn {

        background: #432685;
        padding: 20px;
        color: #fff;
        border-bottom-right-radius: 10px;

        @media screen and (max-width: ${size.mobileL}) {
            padding: 10px;
        }

        button {
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 8px;
            background: #432685;
            outline: none;
            border: none;
            color: #fff;

        }
   }

`

const Box = styled.div`
   margin-top: 100px;
   display: flex;
   align-items: center;
   flex-direction: column;
   justify-content: center;
   padding-bottom: 70px; 

   @media screen and (max-width: ${size.mobileL}) {
        margin-top: 50px;
        padding-bottom: 50px;
   }

   header {
        text-align: center;
        p {
            color: #fb840d;
            font-size: 25px;
            text-transform: uppercase;

            @media screen and (max-width: ${size.mobileL}) {
                font-size: 20px;
                padding-bottom: 10px;
            }
        }
        h3 {
            font-weight: 400;
            font-size: 30px;
            margin-top: -34px;
            text-transform: uppercase;
        }
   }

    .que {
        display: flex;
        gap: 20px;
        align-items: center;
        justify-content: center;
        flex-wrap: wrap;
        width: 90%; 

        @media screen and (max-width: ${size.mobileL}) {
            width: 100%;
            padding-left: 20px;
            padding-right: 20px;
        }

        .que-box {
            padding: 10px 25px;
            border: .5px solid none;
            border-radius: 8px;
            width: 25%;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            cursor: pointer;
            transition: transform 0.3s ease;

            @media screen and (max-width: ${size.mobileL}) {
                width: 80%;
            }
        }

        .que-box:hover {
            color: #fff;
            background: linear-gradient(to top, #432685, #582cc1, #792fcb);
            transform: translateY(-5px);
        }
   }
  
`
 
export default Question