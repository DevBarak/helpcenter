import styled from "styled-components";
import React, { useState } from "react";
import Hero from "./hero";
import Question from "./question";
import Category from "./category";
import Terms from "./terms";
import Footer from "./footer";

const Landing = ({page, setPage}) => {

    return ( 
        <Container>
            <Hero page={page} setPage={setPage} />
            <Question />
            <Category />
            <Terms />
            <Footer />
        </Container>
     );
}

const Container = styled.div`
    width: 100%;
    height: 100%;
    overflow: hidden;
`
 
export default Landing;