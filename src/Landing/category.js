import styled from "styled-components";
import React from "react";
import dep from '../assets/deposit.png'
import draw from '../assets/withdraw.png'
import gen from '../assets/general.png'
import acct from '../assets/account.png'
import { size } from '../breakpoints';

const Category = () => {

    const catarray = [
        {
            image: dep,
            head: "deposit",
            content: "nisi ut aliquet cursus. Suspendisse quis orci at leo tempor dapibus et volutpat urna. Nunc id varius nisi, vel imperdiet ex.",
            foot: "Nivia",
        },
        {
            image: draw,
            head: "withdraw",
            content: "nisi ut aliquet cursus. Suspendisse quis orci at leo tempor dapibus et volutpat urna. Nunc id varius nisi, vel imperdiet ex.",
            foot: "Nivia",
        },
        {
            image: gen,
            head: "general",
            content: "nisi ut aliquet cursus. Suspendisse quis orci at leo tempor dapibus et volutpat urna. Nunc id varius nisi, vel imperdiet ex. ",
            foot: "Nivia",
        },
        {
            image: acct,
            head: "account",
            content: "nisi ut aliquet cursus. Suspendisse quis orci at leo tempor dapibus et volutpat urna. Nunc id varius nisi, vel imperdiet ex.",
            foot: "Nivia",
        },
    ]
    return ( 
        <Con>
            <header>
                <p>Got any questions</p>
                <h3>browse all categories</h3>
            </header>

            <Box>
               <div className="cat">
                    {catarray.map(({image, head, content, foot}) => 
                        <div className="cat-box">
                           <img src={image} alt="img" />
                           <h3>{head}</h3>
                           <p>{content}</p>
                           <h2>{foot}</h2>
                        </div>
                    )}
               </div>
            </Box>
        </Con>
     );
}

const Con = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 70vh;
    background: linear-gradient(to top, #432685, #582cc1, #792fcb); 
    @media screen and (max-width: ${size.mobileL}) {
        height: 100%;
        padding-bottom: 40px;
    }

    header {
        margin-top: 300px;
        text-align: center;
        width: 100%;
        @media screen and (max-width: ${size.mobileL}) {
            margin-top: 80px;
        }

        p {
            color: #fb840d;
            font-size: 30px;
            text-transform: uppercase;

            @media screen and (max-width: ${size.mobileL}) {
                font-size: 20px;
                padding-bottom: 10px;
            }
        }
        h3 {
            font-weight: 400;
            font-size: 40px;
            margin-top: -34px;
            text-transform: uppercase;
            color: #fff;

            @media screen and (max-width: ${size.mobileL}) {
                font-size: 30px;
            }
        }
   }
`

const Box = styled.div`

        display: flex;
        flex-direction: column;
        align-items: center;
        align-items: center;
        justify-content: center;

   .cat {
        display: flex;
        align-items: center;
        align-items: center;
        justify-content: center;
        gap: 25px;
        margin-top: 30px;

        @media screen and (max-width: ${size.mobileL}) {
            flex-direction: column;
        }

        .cat-box {
            width: 15%;
            padding: 50px 20px 10px 20px;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            background: #fff;
            text-align: center;
            cursor: pointer;
            transition: transform 0.3s ease;

            @media screen and (max-width: ${size.mobileL}) {
                width: 80%;
            }

            h3 {
                text-transform: uppercase;
                font-weight: 400;
            }

            p {
                text-align: left;
            }

            h2 {
                font-weight: 400;
                padding-top: 50px;
            }
        }

        .cat-box:hover {
            color: #fff;
            background: linear-gradient(to top, #432685, #582cc1, #792fcb);
            transform: translateY(-5px);
        }
   }
`
 
export default Category;